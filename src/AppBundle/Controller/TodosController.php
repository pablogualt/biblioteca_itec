<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class TodosController extends Controller
{
    /**
     * @Route("/cat/todos")
     */
    public function catTodosActions(){
        $em = $this->getDoctrine()->getManager();

        $libros = $em->getRepository('AppBundle:Libro')->findAll();
        

        return $this->render('libro/mostrar.html.twig', array(
            'libros' => $libros,
        ));
    }
}

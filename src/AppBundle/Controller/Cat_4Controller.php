<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class Cat_4Controller extends Controller
{
    /**
     * @Route("/cat/4")
     */
    public function cat4Actions(){
        $em = $this->getDoctrine()->getEntityManager();

        $query = $em->createQuery("
            
                SELECT l FROM AppBundle:Libro l
                WHERE l.categoria = 5
        ");
        $libros = $query->getResult();
//        foreach ($libros as $libro){
//
//            echo "NOMBRE LIBRO :" .$libro->getNombre()."<br/>";
//        };
//        die();
        return $this->render('libro/mostrar.html.twig', array(
            'libros' => $libros,
        ));
    }
}

<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/admin")
 */
class AdminController extends Controller
{
    /**
     * Lists all libro entities.
     *
     * @Route("/", name="admin_index")
     *
     */
    public function adminAction()
    {
        $em = $this->getDoctrine()->getManager();

        $libros = $em->getRepository('AppBundle:Libro')->findAll();

        return $this->render('admin/index.html.twig', array(
            'libros' => $libros,
        ));
    }

}

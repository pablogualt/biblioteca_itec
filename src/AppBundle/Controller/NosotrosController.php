<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class NosotrosController extends Controller
{
    /**
     * @Route("/nosotros")
     */
    public function nosotrosAction()
    {


        //return this render
        return $this->render('default/nosotros.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ]);

    }
}

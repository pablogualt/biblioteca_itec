<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Subcategoria;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * subcategoria controller.
 *
 * @Route("subcategoria")
 */
class SubcategoriaController extends Controller
{
    /**
     * Lists all subcategoria entities.
     *
     * @Route("/", name="subcategoria_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $subcategorias = $em->getRepository('AppBundle:Subcategoria')->findAll();

        return $this->render('subcategoria/index.html.twig', array(
            'subcategorias' => $subcategorias,
        ));
    }

    /**
     * Creates a new subcategoria entity.
     *
     * @Route("/new", name="subcategoria_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $subcategoria = new subcategoria();
        $form = $this->createForm('AppBundle\Form\SubcategoriaType', $subcategoria);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($subcategoria);
            $em->flush($subcategoria);

            return $this->redirectToRoute('subcategoria_show', array('id' => $subcategoria->getId()));
        }

        return $this->render('subcategoria/new.html.twig', array(
            'subcategoria' => $subcategoria,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a subcategoria entity.
     *
     * @Route("/{id}", name="subcategoria_show")
     * @Method("GET")
     */
    public function showAction(Subcategoria $subcategoria)
    {
        $deleteForm = $this->createDeleteForm($subcategoria);

        return $this->render('subcategoria/show.html.twig', array(
            'subcategoria' => $subcategoria,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing subcategoria entity.
     *
     * @Route("/{id}/edit", name="subcategoria_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Subcategoria $subcategoria)
    {
        $deleteForm = $this->createDeleteForm($subcategoria);
        $editForm = $this->createForm('AppBundle\Form\SubcategoriaType', $subcategoria);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('subcategoria_edit', array('id' => $subcategoria->getId()));
        }

        return $this->render('subcategoria/edit.html.twig', array(
            'subcategoria' => $subcategoria,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a subcategoria entity.
     *
     * @Route("/{id}", name="subcategoria_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Subcategoria $subcategoria)
    {
        $form = $this->createDeleteForm($subcategoria);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($subcategoria);
            $em->flush($subcategoria);
        }

        return $this->redirectToRoute('subcategoria_index');
    }

    /**
     * Creates a form to delete a subcategoria entity.
     *
     * @param Subcategoria $subcategoria The subcategoria entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Subcategoria $subcategoria)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('subcategoria_delete', array('id' => $subcategoria->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
  
}

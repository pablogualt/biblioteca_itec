<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        return $this->render('default/index2.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ]);
    }
    /**
     * @Route("/index2" , name="biblio")
     */
    public function aboutAction()
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('AppBundle:Libro');
        $query = $repository->createQueryBuilder('l')
            ->orderBy('l.fechaCreacion','DESC')
            ->setMaxResults(6)
            ->getQuery();

        $libross1 = $query->getResult();


        $em1 = $this->getDoctrine()->getManager();
        $repository1 = $em1->getRepository('AppBundle:Libro');
        $query1 = $repository1->createQueryBuilder('l')
            ->orderBy('l.fechaCreacion')
            ->setMaxResults(6)
            ->getQuery();

        $libross2 = $query1->getResult();

      

        return $this->render('default/index.html.twig', array(
            'libro' => $libross1,
            'libro2' => $libross2
        ));


        //return this render
//        return $this->render('default/index.html.twig', [
//            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
//        ]);

        
    }
    /**
     * @Route("enviar", name="Envio")
     */
    public function sendEmailAction(Request $request)
    {
        $remitente = $request->request->get("remitente");
        $cuerpo = $request->request->get("mensaje");
        $titulo = $request->request->get("nombre");
        $mensaje = 'de: ' . $titulo . ' de: ' . $remitente . ' mensaje: ' . $cuerpo;


        $message = \Swift_Message::newInstance()
            ->setSubject("no responder pagina web contacto")
            ->setFrom($remitente)
            ->setTo("biblioteca@itecriocuarto.org.ar")
            ->setBody($mensaje);
        $this->get('mailer')->send($message);
        return $this->render(':default:index2.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..') . DIRECTORY_SEPARATOR,
        ]);
    }
    

}

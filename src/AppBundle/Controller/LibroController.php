<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Libro;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Libro controller.
 *
 * @Route("libro")
 */
class LibroController extends Controller
{
    /**
     * Lists all libro entities.
     *
     * @Route("/all", name="libro_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $libros = $em->getRepository('AppBundle:Libro')->findAll();

        return $this->render('libro/index.html.twig', array(
            'libros' => $libros,
        ));
    }

    /**
     * Creates a new libro entity.
     *
     * @Route("/new", name="libro_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $libro = new Libro();
        $form = $this->createForm('AppBundle\Form\LibroType', $libro);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            // $file stores the uploaded PDF file
            // @var Symfony\Component\HttpFoundation\File\UploadedFile $file
            $file = $libro->getArchivo();

            // Generate a unique name for the file before saving it
            $fileName = md5(uniqid()).'.'.$file->guessExtension();

            // Move the file to the directory where brochures are stored
            $file->move(
                $this->getParameter('archivos_directory'),
                $fileName

            );
             // $file stores the uploaded PDF file
            // @var Symfony\Component\HttpFoundation\File\UploadedFile $file
            $file2 = $libro->getImage();

            // Generate a unique name for the file before saving it
            $fileName2 = md5(uniqid()).'.'.$file2->guessExtension();

            // Move the file to the directory where brochures are stored
            $file2->move(
                $this->getParameter('archivos_directory'),
                $fileName2
            );

            // Update the 'brochure' property to store the PDF file name
            // instead of its contents
            $libro->setImage($fileName2);
            $libro->setArchivo($fileName);



            $em = $this->getDoctrine()->getManager();
            $em->persist($libro);
            $em->flush($libro);

            return $this->redirectToRoute('libro_show', array('id' => $libro->getId()));
        }

        return $this->render('libro/new.html.twig', array(
            'libro' => $libro,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a libro entity.
     *
     * @Route("/{id}", name="libro_show")
     * @Method("GET")
     */
    public function showAction(Libro $libro)
    {
        $deleteForm = $this->createDeleteForm($libro);

        return $this->render('libro/show.html.twig', array(
            'libro' => $libro,
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Finds and displays a libro entity.
     *
     * @Route("/mostrar/{id}", name="libro_ver")
     * @Method("GET")
     */
    public function verAction(Libro $libro)
    {
        $deleteForm = $this->createDeleteForm($libro);

        return $this->render('libro/ver.html.twig', array(
            'libro' => $libro,
            'delete_form' => $deleteForm->createView(),
        ));
    }


    /**
     * Displays a form to edit an existing libro entity.
     *
     * @Route("/{id}/edit", name="libro_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Libro $libro)
    {
        $deleteForm = $this->createDeleteForm($libro);
        $editForm = $this->createForm('AppBundle\Form\LibroType', $libro);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('libro_edit', array('id' => $libro->getId()));
        }

        return $this->render('libro/edit.html.twig', array(
            'libro' => $libro,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a libro entity.
     *
     * @Route("/{id}", name="libro_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Libro $libro)
    {
        $form = $this->createDeleteForm($libro);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($libro);
            $em->flush($libro);
        }

        return $this->redirectToRoute('libro_index');
    }

    /**
     * Creates a form to delete a libro entity.
     *
     * @param Libro $libro The libro entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Libro $libro)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('libro_delete', array('id' => $libro->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
    /**
     * @Route("/busca", name="libro_busca")
     */
    public function buscaActions(Request $request){
        $nombre2 = $request->request->get("search");
       $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('AppBundle:Libro');
        $query = $repository->createQueryBuilder('l')
            ->where('l.nombre LIKE :nombre')
            //->orWhere('l.autor LIKE :nombre')
            ->setParameter('nombre', '%'.$nombre2.'%' )
            ->getQuery();
        $libross = $query->getResult();

        return $this->render('libro/mostrar.html.twig', array(
            'libros' => $libross,
        ));
    }
   

}

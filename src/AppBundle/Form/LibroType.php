<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;


class LibroType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('nombre')
            ->add('ano')
            ->add('descripcion')
            ->add('image', FileType::class, array('label' => 'Imagen (imagen file)'))


            ->add('autor')
            ->add('editorial')
            ->add('categoria')
            ->add('User')
            ->add('archivo', FileType::class, array('label' => 'Archivo (PDF file)',
                'data_class' => null));

    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Libro'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_libro';
    }


}

<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Libro
 *
 * @ORM\Table(name="libro")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LibroRepository")
 */
class Libro
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var int
     *
     * @ORM\Column(name="ano", type="integer")
     */
    private $ano;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=2000)
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255)
     */
    private $image;



    /**
     * @ORM\Column(type="datetime", nullable=false)
     * @ORM\Version
     * @var \DateTime
     */
    protected $fechaCreacion =null;

    /**
     * @ORM\Column(type="datetime", columnDefinition="DATETIME on update CURRENT_TIMESTAMP")
     */
    protected $fechaModificacion;

    /**
     * @var int
     *
     * @ORM\Column(name="cantidadDescargas", type="integer")
     */
    protected $cantidadDescargas = 0;



    /**
     * @ORM\ManyToOne(targetEntity="Autor", inversedBy="libros")
     * @ORM\JoinColumn(name="autor_id", referencedColumnName="id")
     */
    private $autor;

    /**
     * @ORM\ManyToOne(targetEntity="Editorial", inversedBy="libros")
     * @ORM\JoinColumn(name="editorial_id", referencedColumnName="id")
     */
    private $editorial;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Categoria", inversedBy="libros")
     * @ORM\JoinColumn(name="categoria_id", referencedColumnName="id")
     */
    private $categoria;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="libros")
     * @ORM\JoinColumn(name="User_id", referencedColumnName="id")
     */
    private $User;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\NotBlank(message="Please, upload the book as a PDF file.")
     * @Assert\File(mimeTypes={ "application/pdf" })
     */
    private $archivo;

    public function __construct()
    {
        $this->fechaCreacion = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * @param mixed $fechaCreacion
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;
    }

    /**


    /**
     * @return mixed
     */
    public function getFechaModificacion()
    {
        return $this->fechaModificacion;
    }

    /**
     * @param mixed $fechaModificacion
     */
    public function setFechaModificacion($fechaModificacion)
    {
        $this->fechaModificacion = $fechaModificacion;
    }

    /**
     * @return int
     */
    public function getCantidadDescargas()
    {
        return $this->cantidadDescargas;
    }

    /**
     * @param int $cantidadDescargas
     */
    public function setCantidadDescargas($cantidadDescargas)
    {
        $this->cantidadDescargas = $cantidadDescargas;
    }




    

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Libro
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set ano
     *
     * @param integer $ano
     *
     * @return Libro
     */
    public function setAno($ano)
    {
        $this->ano = $ano;

        return $this;
    }

    /**
     * Get ano
     *
     * @return int
     */
    public function getAno()
    {
        return $this->ano;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Libro
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }





    /**
     * Set autor
     *
     * @param \AppBundle\Entity\Autor $autor
     *
     * @return Libro
     */
    public function setAutor(\AppBundle\Entity\Autor $autor = null)
    {
        $this->autor = $autor;

        return $this;
    }

    /**
     * Get autor
     *
     * @return \AppBundle\Entity\Autor
     */
    public function getAutor()
    {
        return $this->autor;
    }

    /**
     * Set editorial
     *
     * @param \AppBundle\Entity\Editorial $editorial
     *
     * @return Libro
     */
    public function setEditorial(\AppBundle\Entity\Editorial $editorial = null)
    {
        $this->editorial = $editorial;

        return $this;
    }

    /**
     * Get editorial
     *
     * @return \AppBundle\Entity\Editorial
     */
    public function getEditorial()
    {
        return $this->editorial;
    }

    /**
     * Set Categoria
     *
     * @param \AppBundle\Entity\Categoria $categoria
     *
     * @return Libro
     */
    public function setCategoria(\AppBundle\Entity\Categoria $categoria = null)
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get categoria
     *
     * @return \AppBundle\Entity\Categoria
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * Set fosUser
     *
     * @param \AppBundle\Entity\User $fosUser
     *
     * @return Libro
     */
    public function setFosUser(\AppBundle\Entity\User $fosUser = null)
    {
        $this->fos_user = $fosUser;

        return $this;
    }

    /**
     * Get fosUser
     *
     * @return \AppBundle\Entity\User
     */
    public function getFosUser()
    {
        return $this->fos_user;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Libro
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->User = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->User;
    }


    public function getArchivo()
    {
        return $this->archivo;
    }

    public function setArchivo($archivo)
    {
        $this->archivo = $archivo;

        return $this;
    }
}

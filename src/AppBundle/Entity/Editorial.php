<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Editorial
 *
 * @ORM\Table(name="editorial")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EditorialRepository")
 */
class Editorial
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;


    /**
     * Get id
     *
     * @return int
     */


    /**
     * @ORM\OneToMany(targetEntity="Editorial", mappedBy="libro")
     */
    private $Editorial;

    public function __construct()
    {
        $this->Editorial = new ArrayCollection();
    }






    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Editorial
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Add editorial
     *
     * @param \AppBundle\Entity\Editorial $editorial
     *
     * @return Editorial
     */
    public function addEditorial(\AppBundle\Entity\Editorial $editorial)
    {
        $this->Editorial[] = $editorial;

        return $this;
    }

    /**
     * Remove editorial
     *
     * @param \AppBundle\Entity\Editorial $editorial
     */
    public function removeEditorial(\AppBundle\Entity\Editorial $editorial)
    {
        $this->Editorial->removeElement($editorial);
    }

    /**
     * Get editorial
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEditorial()
    {
        return $this->Editorial;
    }
    public function __toString()
    {
        return (string) $this->nombre;

    }
}

<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Autor
 *
 * @ORM\Table(name="autor")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AutorRepository")
 */
class Autor
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="apellido", type="string", length=255)
     */
    private $apellido;


    /**
     * Get id
     *
     * @return int
     */


    
    /**
     * @ORM\OneToMany(targetEntity="Autor", mappedBy="libro")
     */
    private $Autores;

    public function __construct()
    {
        $this->Autores = new ArrayCollection();
    }




    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Autor
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellido
     *
     * @param string $apellido
     *
     * @return Autor
     */
    public function setApellido($apellido)
    {
        $this->apellido = $apellido;

        return $this;
    }

    /**
     * Get apellido
     *
     * @return string
     */
    public function getApellido()
    {
        return $this->apellido;
    }

    /**
     * Add autore
     *
     * @param \AppBundle\Entity\Autor $autore
     *
     * @return Autor
     */
    public function addAutore(\AppBundle\Entity\Autor $autore)
    {
        $this->Autores[] = $autore;

        return $this;
    }

    /**
     * Remove autore
     *
     * @param \AppBundle\Entity\Autor $autore
     */
    public function removeAutore(\AppBundle\Entity\Autor $autore)
    {
        $this->Autores->removeElement($autore);
    }

    /**
     * Get autores
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAutores()
    {
        return $this->Autores;
    }
    public function __toString()
    {
        return (string) $this->nombre.' '.$this->apellido;

    }
}

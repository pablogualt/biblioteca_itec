<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Categoria
 *
 * @ORM\Table(name="categoria")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CategoriaRepository")
 */
class Categoria
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;


    /**
     * Get id
     *
     * @return int
     */


    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Categoria", mappedBy="libro")
     */
    private $Categorias;

    public function __construct()
    {
        $this->Categorias = new ArrayCollection();
    }



    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Categoria
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Add categoria
     *
     * @param \AppBundle\Entity\Categoria $categoria
     *
     * @return Categoria
     */
    public function addCategoria(\AppBundle\Entity\Categoria $categoria)
    {
        $this->Categorias[] = $categoria;

        return $this;
    }

    /**
     * Remove categoria
     *
     * @param \AppBundle\Entity\Categoria $categoria
     */
    public function removeCategoria(\AppBundle\Entity\Categoria $categoria)
    {
        $this->Categorias->removeElement($categoria);
    }

    /**
     * Get categorias
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategorias()
    {
        return $this->Categorias;
    }
    public function __toString()
    {
        return (string) $this->nombre;

    }
}

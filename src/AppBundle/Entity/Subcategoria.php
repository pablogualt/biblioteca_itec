<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Subcategoria
 *
 * @ORM\Table(name="subcategoria")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SubcategoriaRepository")
 */
class Subcategoria
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;


    /**
     * @ORM\ManyToOne(targetEntity="Categoria", inversedBy="subcategorias")
     * @ORM\JoinColumn(name="categoria_id", referencedColumnName="id")
     */
    private $categoria;

    

    /**
     * @ORM\OneToMany(targetEntity="Subcategoria", mappedBy="libro")
     */
    private $Subcategory;

    public function __construct()
    {
        $this->Subcategory = new ArrayCollection();
    }
    
    

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Subcategoria
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Add subcategoria
     *
     * @param \AppBundle\Entity\Subcategoria $subcategoria
     *
     * @return Subcategoria
     */
    public function addSubcategoria(\AppBundle\Entity\Subcategoria $subcategoria)
    {
        $this->Subcategory[] = $subcategoria;

        return $this;
    }

    /**
     * Remove subcategoria
     *
     * @param \AppBundle\Entity\Subcategoria $subcategoria
     */
    public function removeSubcategoria(\AppBundle\Entity\Subcategoria $subcategoria)
    {
        $this->Subcategory->removeElement($subcategoria);
    }

    /**
     * Get subcategorias
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubcategory()
    {
        return $this->Subcategory;
    }

    /**
     * Set categoria
     *
     * @param \AppBundle\Entity\Categoria $categoria
     *
     * @return Subcategoria
     */
    public function setCategoria(\AppBundle\Entity\Categoria $categoria = null)
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get categoria
     *
     * @return \AppBundle\Entity\Categoria
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    public function __toString()
    {
        return (string) '('.$this->id.')'.$this->getNombre();
        // TODO: Implement __toString() method.
    }
}
